# Build

## Prerequisites
* This assumes you have docker installed
* Set your ```/etc/hosts``` add the line ```127.0.0.1       www.rainbowconnection.com```

## Easy Way

* ```docker pull chewbackhaus/rainbow-connections-client:2.0```
* ```docker run --name rainbow-connetion-client -d -p 80:4200 -p 49153:49153 --rm chewbackhaus/rainbow-connections-client:2.0 /var/www/client/run-ember.sh```

## Harder Way
* I can invite you to the repo to clone ```git clone https://bitbucket.org/chewbackhaus/rainbow-connection-client/overview```
* Run ```./bash/start-front-end-dev.sh``` Be sure to set the path to your deployment
* ```cd client```
* ```ember serve```

## That's it! (Hopefully ;D)
* Visit ```www.rainbowconnection.com``` to click around
* Make sure the server is running [here](https://bitbucket.org/chewbackhaus/rainbow-connection-server/overview)

# Architecture and Completeness

## Completeness

Here are the items I wasn't able to

* ```Clicking a list item's remove button should remove that connection and update the current view.```
  * You'll get an alert with a TODO with the id at the moment
* ```Clicking on the favorite color of the current user in the title bar should give a drop-down selection of colors.  Selecting a new color should update the current user's color.```
  * Ran out of time on this one
* ```Tertiary Colors```
  * Skipped this to focus on other stuff
* ```infinite pagination```
  * Wrote most of the backend code for this to work, but didn't get the client work done in time

## Architecture Decisions

### Two Projects

I decided to split this into two separate projects, ```rainbow-connection-client``` and ```rainbow-connection-server```.  They're both on my bitbucket page.  Given then I've never done ANYTHING with either of these
frameworks, it seemed a bit easier/quicker for me have clean slates rather than trying to find the best
way to have them play nice together.

### Ember

I don't really feel like I got the modeling completely right with Ember.  Towards, the end, I was making it work, and there are definitely things that need to be cleaned up.  I had a lot of
code in the route files that should probalby move.  The data models didn't work so well for the large amount of loading for some of the views, and I believe there
are ways to do this more elegantly in Ember, but I ddin't have time to dig into it.  For the main view with all the users
and connections, I had to really 'fit' it in.  Anyhow, learned a lot, and would do a lot of things differently there

### Backend

Figured I'd put this here to have it all in one place for the sake of the exercise.

#### Users have Inbound and Outbound Connections

This is similar to a 'friend_request' style.  It takes more memory, and opens the door for some inconsistency issues, but I felt like making the assumption that I could use either of
 the two userIds in the user connection relationship might make the code a little harder to read and felt like it would get confusing

#### Tables
There are two tables, ```user``` and ```connections``` tables;  They do what you would expect, and maybe the big decision was whether or not to
 create one entry given they are bi-directional.  As I mention above, in the end, I decided not to

#### Services and Adapters
I have two Services and two Adapters that the Controllers use to interact with the data.  The ```Adapter```
classes were meant to be the main interaction with the databases.  The ```Service``` classes
 are meant to have most of the domain logic.  In this case, it was fairly straightforward, so they seem a bit redundant,
 but that's ok.

#### Performance
I would want to go back and really spend some time with the queries I built in laravel. I believe I have the indexes set up
correctly, but I honestly didn't have a ton of time to test all the cases

There are a few places that I would definitely want to spend some time with optimizing.  i.e. loading too many users
instead of using batch

#### Validation and Error Handling

There are a LOT of places where I should be doing better validation, but a TODO is good enough right!?

There are I'm sure are plenty of edge cases and bugs that I would liked to have worked on more

#### Unit Tests

The coverage is OK, but definitely some of the API controllers and some of the service/adapter code needs testing

# TestData

As you mentioned, you can hit the ```www.rainbowconnection/testdata``` with the ```userCount``` POST param set in the body (as specified).  I was using PostMan for this