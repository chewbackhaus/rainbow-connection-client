#!/usr/bin/env bash

docker run --name rainbow-connetion-client -d -p 80:4200 -p 49153:49153 --rm chewbackhaus/rainbow-connections-client:2.0 /var/www/client/run-ember.sh