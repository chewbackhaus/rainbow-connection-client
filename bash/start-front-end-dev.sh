#!/usr/bin/env bash

export DEV_LOCATION=/Users/mybestday/Documents/Workspace/rainbow-connection-client
echo $DEV_LOCATION;
docker run --name client-dev -ti -v $DEV_LOCATION:/var/www -p 80:4200 -p 49153:49153 --rm chewbackhaus/rainbow-connections-client:2.0 /bin/bash

#docker run --name client-dev -ti -p 80:4200 -p 49153:49153 --rm chewbackhaus/rainbow-connections-client:2.0 /bin/bash