import DS from 'ember-data';

export default DS.JSONAPIAdapter.extend({
  host: 'http://localhost:8080/api',
  urlForQuery (query, modelName) {
    switch(modelName) {
      case 'user':
        return 'http://localhost:8080/api/users/' + query + '/connections';
      default:
        return this._super(...arguments);
    }
  },

  urlForQueryRecord (query, modelName) {
    switch(modelName) {
      case 'all-user':
        return 'http://localhost:8080/api/users';
      default:
        return this._super(...arguments);
    }
  }
});


