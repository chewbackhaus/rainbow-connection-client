import Ember from 'ember';

export default Ember.Controller.extend({
  init: function() {
    console.log('init');


  },

  actions: {
    listItemClick() {
      event.stopPropagation();
      event.preventDefault();

      var target = event.target;
      if (!target || !target.dataset.removeUserId) {
        console.log('no one to remove');
        return;
      }

      alert('TODO Remove User Id: ' + target.dataset.removeUserId);
    }
  }
});
