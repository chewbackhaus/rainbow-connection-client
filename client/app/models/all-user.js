import DS from 'ember-data';

/**
 *     {
        'data' =>
            'selected_user_refs' => [
                '43',
                '26',
                '85'
    ],
        'user_objects' => [ {
            "type": "people",
            "id": "123",
            "attributes": {
            "first-name": "Jeff",
            "last-name": "Atwood"
            'connection_user_refs' => [
                '1',
                '2'
            ]
        },{
            "type": "people",
            "id": "123",
            "attributes": {
            "first-name": "Jeff",
            "last-name": "Atwood"
        } ]
    }
 */

export default DS.Model.extend({
  'selectedUserRefs'  : DS.attr(),
  'userObjects'   : DS.attr(),
  'usersToShow'   : DS.attr()
  // 'username' : DS.attr()
});
