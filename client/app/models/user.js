import DS from 'ember-data';

export default DS.Model.extend({
  'first-name'  : DS.attr(),
  'last-name'   : DS.attr(),
  'fav-color-hex' : DS.attr(),
  'user-connections' : DS.hasMany('userconnection')
});
