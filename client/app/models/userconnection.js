import DS from 'ember-data';

/**
 * Object is pretty much the same as User, but was tired of fighting with ember
 */
export default DS.Model.extend({
  'first-name'  : DS.attr(),
  'last-name'   : DS.attr(),
  'fav-color-hex' : DS.attr()
});
