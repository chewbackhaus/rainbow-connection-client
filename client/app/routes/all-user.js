import Ember from 'ember';

export default Ember.Route.extend({

  /**** ############################
   *
   * I KNOW THIS FUNCTION IS NASTY - PRETTY SURE IT DOESNT BELONG HERE, BUT TIMES A TICKIN!
   *
   * @param params
   * @returns {*|Promise.<TResult>}
   */


  model(params) {
    var store = this.get('store');
    return this.get('store').queryRecord('all-user', { page: 1 }).then(function(allUserResponse) {
      var userObjectJsonArr = allUserResponse.get('userObjects');

      var selectedUserRefsArr = {};
      //push all data objects into the user store so we can reference them later
      for (var key in userObjectJsonArr) {
        if (!userObjectJsonArr.hasOwnProperty(key)) continue;

        store.pushPayload({
          'data' : userObjectJsonArr[key]
        });

        selectedUserRefsArr[key] = userObjectJsonArr[key]['connection-user-refs'];
      }

      /**
       * Will hold the users we will show in our model
       *
       * @type {Array}
       */
      var usersToShow = [];

      /**
       * Go through each of userRefs and userConnectionRefs and build the models
       * with the populated model objects
       */
      allUserResponse.get('selectedUserRefs').forEach(function(userRef) {
        var user = store.peekRecord('user', userRef);

        var userConnectionsArr = [];
        selectedUserRefsArr[userRef].forEach(function(userConnectionRef) {

          var userConnection = store.peekRecord('userconnection', userConnectionRef);
          if (userConnection) {
            userConnectionsArr.push(userConnection);
            return;
          }

          var user = store.peekRecord('user', userConnectionRef);
          if (!user) {
            //not in the store!
            //TODO handle error case but just skip for now
            return;
          }

          /**
           * Create a new record for now
           */
          userConnectionsArr.push(store.createRecord('userconnection', {
            'first-name' : user.get('first-name'),
            'last-name' : user.get('last-name'),
            'fav-color-hex' : user.get('fav-color-hex'),
            'id' : user.get('id')
          }));
        });

        user.set('user-connections', userConnectionsArr);
        usersToShow.push(user);
      });

      allUserResponse.set('usersToShow', usersToShow);
      return allUserResponse;
    });
  }
});
