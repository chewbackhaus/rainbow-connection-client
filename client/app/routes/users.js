import Ember from 'ember';

export default Ember.Route.extend({

  model(params) {
    console.log(params.user_id);

    var self = this;
    return this.get('store').find('user', params.user_id).then(function(user) {
      self.get('store').query('user', params.user_id).then(function(userconnections) {
        // return users;
        // debugger;
        user.set('user-connections', userconnections);
      });

      return user;
    });
  }
});
