import DS from 'ember-data';

export default DS.JSONSerializer.extend({
  normalizeQueryRecordResponse(store, type, payload) {
    // debugger;

    return {
      data: {
        id : Date.now(),
        type : 'all-user',
        attributes: {
          selectedUserRefs : payload.data['selected-user-refs'],
          userObjects : payload.data['user-objects']
        }
      }
    };

    // return {
    //   data: {
    //     id: payload.login,
    //     type: type.modelName,
    //     attributes: {
    //       name: payload.name,
    //       publicRepos: payload.public_repos
    //     }
    //   }
    // }
  },

  extractErrors(store, typeClass, payload, id) {
    debugger;
    if (payload && typeof payload === 'object' && payload._problems) {
      payload = payload._problems;
      this.normalizeErrors(typeClass, payload);
    }
    return payload;
  }
});
